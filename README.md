# Quantbase frontend challenge

## CTACard

`<CTACard />` component provides basic layout for the card with some call to action. By default it provides two variants, but it can be extended through custom css styles, that can be passed as `className` or `style` props.
