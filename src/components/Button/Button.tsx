import { HTMLAttributes } from 'react'
import { Link } from 'react-router-dom'
import { cn } from '../../utils/cn'

import styles from './Button.module.css'

interface IButtonProps {
	className?: string
	to?: string
	role?: 'button' | 'submit'
	variant?: 'primary' | 'secondary'
}

export const Button: React.FC<IButtonProps> = ({
	className,
	children,
	to,
	variant = 'primary',
	role
}) => {
	if (to) {
		return (
			<Link className={cn(styles.button, styles[variant], className)} to={to}>
				{children}
			</Link>
		)
	}

	return <button role="role">{children}</button>
}
