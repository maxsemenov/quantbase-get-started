import { cn } from '../../utils/cn'
import styles from './CTACard.module.css'

interface ICTACardProps {
	body?: React.ReactNode
	className?: string
	cta: React.ReactNode
	style?: React.CSSProperties
	subtitle?: React.ReactNode
	title: React.ReactNode
	variant?: 'primary' | 'secondary'
}

export const CTACard: React.FC<ICTACardProps> = ({
	body,
	className,
	cta,
	style,
	subtitle,
	title,
	variant = 'primary'
}) => {
	return (
		<section
			className={cn(styles.card, styles[variant], className)}
			style={style}
		>
			<header className={styles.heading}>
				<h2>{title}</h2>
				{Boolean(subtitle) && <div>{subtitle}</div>}
			</header>
			{Boolean(body) && <main className={styles.body}>{body}</main>}
			<footer className={styles.footer}>{cta}</footer>
		</section>
	)
}
