import { IUser } from 'Types'

interface IUseCurrentUser {
	data: IUser
}

export const useCurrentUser = (): IUseCurrentUser => {
	return {
		data: {
			firstName: 'Patrick',
			lastName: 'Melrose',
			status: {
				isApplicationComplete: false,
				isVerified: false,
				isFundingConnected: false
			}
		}
	}
}
