declare module 'Types' {
	interface IUserStatus {
		isApplicationComplete: boolean
		isVerified: boolean
		isFundingConnected: boolean
	}

	export interface IUser {
		firstName: string
		lastName: string
		status: IUserStatus
	}
}
