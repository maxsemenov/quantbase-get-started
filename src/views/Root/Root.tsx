import { NavLink, Outlet } from 'react-router-dom'
import { useCurrentUser } from '../../modules/user'
import logoUrl from './quantbaseLogo.svg'
import styles from './Root.module.css'

export const Root: React.FC = () => {
	const { data: user } = useCurrentUser()
	return (
		<>
			<header className={styles.header}>
				<img className={styles.logo} src={logoUrl} role="presentation" />
				<nav className={styles.nav}>
					<ul>
						<li>
							<NavLink to="/funds">Funds</NavLink>
						</li>
						<li>
							<NavLink to="/dashboard">Dashboard</NavLink>
						</li>
						<li>
							<NavLink to="/profile">Hi, {user.firstName}</NavLink>
						</li>
					</ul>
				</nav>
			</header>
			<main>
				<Outlet />
			</main>
		</>
	)
}
