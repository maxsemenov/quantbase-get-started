import styles from './Funds.module.css'

export const Fudns = () => {
	return (
		<section>
			<h2 className={styles.heading}>
				Automated funds for everything high-risk
			</h2>
			<ul className={styles.funds}>
				<li>
					<h3>Crypto 15 Index</h3>
				</li>
				<li>
					<h3>Crypto 30 Index</h3>
				</li>
				<li>
					<h3>Crypto Blue Chip Fund</h3>
				</li>
			</ul>
		</section>
	)
}
