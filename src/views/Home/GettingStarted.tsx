import { Button } from '../../components/Button/Button'
import { CTACard } from '../../components/CTACard/CTACard'
import { useCurrentUser } from '../../modules/user'
import styles from './GettingStarted.module.css'

import formImgUrl from './form.png'

export const GettingStarted = () => {
	const { data: user } = useCurrentUser()

	return (
		<div className={styles.container}>
			{!user.status.isApplicationComplete && (
				<CTACard
					className={styles.applicationCta}
					title="Complete Your Brokerage Application"
					body={
						<>
							<p>
								Before you can start investing we need a few bits of information
								about you.
							</p>
							<p>
								It usually takes about 2 minutes to fill the form, and you might
								need your SSN card and goverment issued ID.
							</p>
							<img src={formImgUrl} role="presentation" />
						</>
					}
					cta={
						<Button
							className={styles.applicationCtaButton}
							to="/brokerage-application?source=home-cta"
						>
							Complete Application
						</Button>
					}
				/>
			)}
			{!user.status.isFundingConnected && (
				<CTACard
					className={styles.bankCta}
					title="Connect Your Bank"
					subtitle="Next steps"
					body={
						<>
							<p>
								Connect your bank account for fast and easy deposits and
								withdraws.
							</p>
							<p>It takes less than a minute and is simple and secure.</p>
						</>
					}
					cta={
						<Button to="/connect-bank?source=home-cta" variant="secondary">
							Connect Bank
						</Button>
					}
					variant="secondary"
				/>
			)}
		</div>
	)
}
