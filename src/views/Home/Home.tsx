import { Fudns } from './Funds'
import { GettingStarted } from './GettingStarted'
import styles from './Home.module.css'

export const Home: React.FC = () => {
	return (
		<div className={styles.container}>
			<GettingStarted />
			<Fudns />
		</div>
	)
}
