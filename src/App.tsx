import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Home } from './views/Home/Home'
import { Root } from './views/Root/Root'

export const App = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Root />}>
					<Route path="/" element={<Home />} />
				</Route>
			</Routes>
		</BrowserRouter>
	)
}
